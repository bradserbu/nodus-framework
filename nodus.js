#!/usr/bin/env node
const NODUS = Component();
Require = require;
_ = require('lodash');
const Logger = require('signale');
Logger.config({
    "displayScope": true,
    "displayBadge": true,
    "displayDate": false,
    "displayFilename": false,
    "displayLabel": true,
    "displayTimestamp": false,
    "underlineLabel": false,
    "underlineMessage": false,
    "underlinePrefix": false,
    "underlineSuffix": false,
    "uppercaseLabel": false
});
function Component(id){
    const $ = require('highland');
    const _ = require('lodash');
    const Promise = require('bluebird');
    const {resolve} = require('bluebird');
    const EventEmitter = require('eventemitter2');
    const RAM = require('random-access-memory');
    const Feed = require('hypercore');
    const feed = Feed(RAM, id, {
        valueEncoding: 'utf-8'
    });
    const Encode = (...data) => JSON.stringify(data);
    const Send = (...data) => new Promise((resolve, reject) => {
        Logger.await('send', data);
        const payload = Encode(...data);
        feed.append(payload, (error, id) => {
            if (error) (
                Logger.fail('send', data, '->', id),
                    reject(error)
            );
            else (
                Logger.success('send', data, '->', id),
                    resolve(id)
            );
        });
    });
    const Match = (...events) => (data) => {
        // Logger.debug('events', events);
        // Logger.debug('data', data);
        if (events.length > data.length)
            return [false, data];

        let matches = true;
        for (let lcv = 0; lcv < events.length; lcv++) {
            if (events[lcv] !== data[lcv]) {
                matches = false;
                break;
            }
        }

        // if (matches) Logger.debug('matches', [matches, events, data]);
        return [matches, data];
    };
    const Stream = encoding => $(feed.createReadStream({
        valueEncoding: encoding,
        live: true
    }));
    const On = (...data) => {
        Logger.info('on', data);
        const event = _.initial(data);
        const action = _.last(data);
        // Logger.debug('events', events);
        // Logger.debug('action', action);
        return Stream('json')
            .map(Match(...event))
            .filter(([matches]) => matches)
            .map(([matches, data]) => data.slice(event.length))
            .each(([...data]) => {
                // Logger.await('on', event, data, '->', action);
                // Logger.info('on', data, '->', action);
                // Logger.await('run', );
                Logger.debug('on', event, data, '->', action);
                const result = action(...data);
                // Logger.success('on', event, data, '->', action);
            });
    };
    const Receive = (...events) => {
        // Logger.info('receive')
        Logger.await('receive', events);
        // Logger.time('receive', events);
        return Stream('json')
            .map(Match(...events))
            .filter(([matches]) => matches)
            .map(([matches, data]) => data.slice(events.length))
            .map(([data]) => data)
            .head()
            .toPromise(Promise)
            .tap((...data) => {
                Logger.success('receive', events, data);
                // Logger.timeEnd('receive', events);
            });
    };

    feed.on('ready', () => {
        // ID: Buffer -> Hex (String)
        const id = feed.key.toString('hex');
        Send('ready', id);
    });

    return {
        Send,
        Receive,
        On
    }
}
const {On, Send, Receive} = NODUS;
const Option = (
    On('option', (...option) => Send(...option)),
        On('options', (...options) => _.each(options, (...option) => Send('option', ...option))),
        (...option) => Send('option', ...option)
);
// ** CLI -> Args -> Send (aka: nodus-run)
On('cli', 'args', (...data) => Send(...data));
On('cli', 'option', (...option) => Send('option', ...option));

// ** Logging
const Log = (
    On('log', (...data) => console.error(data)),
        (...data) => Send('log', data)
);
// ** Display
const Print = (
    On('print', (...data) => console.log(...data)),
        (...data) => Send('print', ...data)
);
const Resource = (...data) => {
    // ** TODO: Repository -> Resource
    const id = Send('resource', ...data);
    const {resolve} = Promise;

    const send = (...data) => resolve(id).then(id => Send(id, ...data));
    const on = (...data) => resolve(id).then(id => On(id, ...data));
    const receive = (...data) => resolve(id).then(id => On(id, ...data));
    const get = (...data) => Receive('set', ...data);
    const set = (...data) => Send('set', ...data);

    return {};
};
const Trace = (
    //On('trace', '!log', (...trace) => On(...trace, (...data) => Log(...trace, data))),
    On('trace', (...trace) =>
        On(...trace, (...data) =>
            console.error(trace, data))),
        (...data) =>
            Send('trace', ...data)
);

const Get = (...data) => Receive('set', ...data);
const Set = (...data) => Send('set', ...data);

// ** Run -> Parse -> CLI -> Args
On('ready', () => {
    const yargs = require('yargs');
    const argv = yargs.argv;
    const args = argv._.slice();
    Send('cli', 'args', ...args); // cli.args = args;

    const options = _.toPairs(_.omit(argv, '_', '$0'));
    _.each(options, option => Option(...option));
});

// Trace();
// Trace('run');
// Trace('cli');
// Trace('run');
// ** Hello World
// On('run', 'hello-world', (...data) => Send('print', 'Hello', data, '!'));
// ** Hello World
// Get('user.name').then((...data) => Send('run', 'hello-world', ...data));
// On('version', () => Send('select', '.version', ['json', ['file', 'contents', ['path', 'join', [__dirname, '..', 'package.json']]]]));
// const Type = (type, ...data) => (
//     On(type, (...type) => Send(...data, ...type)),
//         (...data) => Send(type, ...data)
// );
//
// const Error = Type('error', 'log');
// Error('ERROR', 'Invalid Argument');

// const Error = (
//     On('error', error => Send('log', error)),
//         (...error) => Send('error', error)
// );

// Throw('TEST_ERROR');
